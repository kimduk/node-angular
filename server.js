/**
 * Created by kimduk on 22.05.14.
 */
var express = require('express'),
    http = require('http'),

    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',

    app = express(),
    config = require('./server/config/config')[env];

require('./server/config/express')(app, config);
require('./server/config/mongoose')(config);
require('./server/config/passport')();
require('./server/config/routes')(app);
//test comment
http.createServer(app).listen(config.port, function () {
    console.log('Express server listening on port ' + config.port);
});