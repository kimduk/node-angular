/**
 * Created by kimduk on 27.05.14.
 */

var path = require('path'),
    rootPath = path.normalize(__dirname + '/../../');

module.exports = {
    development: {
        rootPath: rootPath,
        db: 'mongodb://localhost/node-angular',
        port: process.env.PORT || 3000
    },
    production:{
        rootPath: rootPath,
        db: 'mongodb://kimduk:kolipo11@ds033069.mongolab.com:33069/node-angular',
        port: process.env.PORT || 80
    }
}