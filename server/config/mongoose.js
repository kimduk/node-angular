/**
 * Created by kimduk on 27.05.14.
 */

var mongoose = require('mongoose'),
    userModel = require('../models/User'),
    articleModel = require('../models/Article');

module.exports = function (config) {
    mongoose.connect(config.db);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error... '));
    db.once('open', function callback() {
        console.log('node-angular db opened');
    });

    userModel.createDefaultUsers();
    articleModel.createDefaultArticles();
}