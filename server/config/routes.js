/**
 * Created by kimduk on 27.05.14.
 */

var auth = require('./auth'),
    users = require('../controllers/users'),
    articles = require('../controllers/articles'),
    mongoose = require('mongoose'),
    User = mongoose.model('User');

module.exports = function (app) {

    app.get('/api/users', auth.requiresRole('admin'), users.getUser);
    app.post('/api/users', users.createUser);
    app.put('/api/users', users.updateUser);

    app.get('/api/articles', articles.getArticles);
    app.get('/api/articles/:id', articles.getArticleById);
    app.get('/partials/*', function (req, res) {
        res.render('../../public/app/' + req.params[0]);
    });

    app.post('/login', auth.authenticate);

    app.post('/logout', function (req, res) {
        req.logout();
        res.end();
    });

    app.all('/api/*', function(req, res) {
        res.send(404);
    });

    app.get('*', function (req, res) {
        res.render('index', {
            bootstrappedUser: req.user
        });
    });
}