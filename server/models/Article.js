/**
 * Created by kimduk on 04.06.14.
 */

var mongoose = require('mongoose');

var articleSchema = mongoose.Schema({
    title: {type: String, required: '{PATH} is required!'},
    featured: {type: Boolean, required: '{PATH} is required!'},
    published: {type: Date, required: '{PATH} is required!'},
    tags: [String]
});

var Article = mongoose.model('Article', articleSchema);

function createDefaultArticles() {
    Article.find({}).exec(function(err, collection) {
        if (collection.length === 0) {
            Article.create({title: 'Article 1', featured: true, published: new Date('6/5/2014'), tags: ['Info']}),
            Article.create({title: 'Article 2', featured: true, published: new Date('6/3/2014'), tags: ['Main']}),
            Article.create({title: 'Article 3', featured: true, published: new Date('6/1/2014'), tags: ['Info', 'City']}),
            Article.create({title: 'Article 4', featured: true, published: new Date('5/30/2014'), tags: ['City']}),
            Article.create({title: 'Article 5', featured: false, published: new Date('5/26/2014'), tags: ['Tag']}),
            Article.create({title: 'Article 6', featured: true, published: new Date('5/23/2014'), tags: ['Info']}),
            Article.create({title: 'Article 7', featured: false, published: new Date('5/19/2014'), tags: ['Info']}),
            Article.create({title: 'Article 8', featured: true, published: new Date('5/17/2014'), tags: ['Main']}),
            Article.create({title: 'Article 9', featured: true, published: new Date('4/28/2014'), tags: ['Main', 'Info']}),
            Article.create({title: 'Article 10', featured: false, published: new Date('4/18/2014'), tags: ['Tag']}),
            Article.create({title: 'Article 11', featured: true, published: new Date('4/4/2014'), tags: ['Info', 'Main', 'Tag']}),
            Article.create({title: 'Article 12', featured: true, published: new Date('4/3/2014'), tags: ['Info']}),
            Article.create({title: 'Article 13', featured: true, published: new Date('4/1/2014'), tags: ['Main']}),
            Article.create({title: 'Article 14', featured: true, published: new Date('3/30/2014'), tags: ['City']}),
            Article.create({title: 'Article 15', featured: false, published: new Date('3/26/2014'), tags: ['City']}),
            Article.create({title: 'Article 16', featured: true, published: new Date('3/22/2014'), tags: ['Info']})
        }
    })
}

exports.createDefaultArticles = createDefaultArticles;