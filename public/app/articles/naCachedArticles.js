/**
 * Created by kimduk on 05.06.14.
 */

angular.module('app').factory('naCachedArticles', function(naArticles) {
  var articleList;

    return {
        query: function () {
            if (!articleList) {
                articleList = naArticles.query();
            }

            return articleList;
        }
    }
})