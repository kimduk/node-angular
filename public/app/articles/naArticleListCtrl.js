/**
 * Created by kimduk on 04.06.14.
 */

angular.module('app').controller('naArticleListCtrl', function($scope, naCachedArticles) {
    $scope.articles = naCachedArticles.query();

    $scope.sortOptions = [{value: "title", text: "Sort By Title"},
        {value: "published", text: "Sort By publish date"}];

    $scope.sortOrder = $scope.sortOptions[0].value;
})