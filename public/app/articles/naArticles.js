/**
 * Created by kimduk on 04.06.14.
 */

angular.module('app').factory('naArticles', function ($resource) {
    var ArticlesResource = $resource('/api/articles/:_id', {_id: "@id"}, {
        update: {method: 'PUT', isArray: false}
    });

    return ArticlesResource;

})