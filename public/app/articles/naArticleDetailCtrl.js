/**
 * Created by kimduk on 05.06.14.
 */

angular.module('app').controller('naArticleDetailCtrl', function ($scope, naCachedArticles, $routeParams) {
    naCachedArticles.query().$promise.then(function (collection) {
        collection.forEach(function (article) {
            if (article._id === $routeParams.id) {
                $scope.article = article;
            }
        })
    })
})