/**
 * Created by kimduk on 28.05.14.
 */

angular.module('app').value('naToastr', toastr);

angular.module('app').factory('naNotifier', function (naToastr) {
    return {
        notify: function (msg) {
            naToastr.success(msg);
            console.log(msg);
        },
        error: function (msg) {
            naToastr.error(msg);
            console.log(msg);
        }
    }
});