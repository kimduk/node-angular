angular.module('app', ['ngResource', 'ngRoute']);

angular.module('app').config(function($routeProvider, $locationProvider) {
    var routeRoleChecks = {
        admin: {auth: function(naAuth) {
            return naAuth.authorizeCurrentUserForRoute('admin')
        }},
        user: {auth: function(naAuth) {
            return naAuth.authorizeAuthenticatedUserForRoute()
        }}
    }

    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', { templateUrl: '/partials/main/main', controller: 'naMainCtrl'})
        .when('/admin/users', { templateUrl: '/partials/admin/user-list',
            controller: 'naUserListCtrl', resolve: routeRoleChecks.admin
        })
        .when('/signup', { templateUrl: '/partials/account/signup',
            controller: 'naSignupCtrl'
        })
        .when('/profile', { templateUrl: '/partials/account/profile',
            controller: 'naProfileCtrl', resolve: routeRoleChecks.user
        })
        .when('/articles', { templateUrl: '/partials/articles/article-list',
            controller: 'naArticleListCtrl'
        })
        .when('/articles/:id', { templateUrl: '/partials/articles/article-details',
            controller: 'naArticleDetailCtrl'
        })

});

angular.module('app').run(function($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function(evt, current, previous, rejection) {
        if(rejection === 'not authorized') {
            $location.path('/');
        }
    })
})