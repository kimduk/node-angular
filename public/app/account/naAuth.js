/**
 * Created by kimduk on 28.05.14.
 */

angular.module('app').factory('naAuth', function ($http, naIdentity, $q, naUser) {
    return {
        authenticateUser: function (username, password) {
            var dfd = $q.defer();
            $http.post('/login', {username: username, password: password}).then(function (response) {
                if (response.data.success) {
                    var user = new naUser();
                    angular.extend(user, response.data.user);
                    naIdentity.currentUser = user;
                    dfd.resolve(true);
                } else {
                    dfd.resolve(false);
                }
            });

            return dfd.promise;
        },
        logoutUser: function () {
            var dfd = $q.defer();
            $http.post('/logout', {logout: true}).then(function () {
                naIdentity.currentUser = undefined;
                dfd.resolve();
            });

            return dfd.promise;
        },
        authorizeCurrentUserForRoute: function (role) {
            if (naIdentity.isAuthorized(role)) {
                return true;
            } else {
                return $q.reject('not authorized');
            }
        },
        authorizeAuthenticatedUserForRoute: function () {
            if (naIdentity.isAuthenticated()) {
                return true;
            } else {
                return $q.reject('not authorized');
            }
        },
        createUser: function (newUserData) {
            var newUser = new naUser(newUserData);
            var dfd = $q.defer();

            newUser.$save().then(function () {
                naIdentity.currentUser = newUser;
                dfd.resolve();
            }, function (response) {
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        },
        updateCurrentUser: function (newUserData) {
            var dfd = $q.defer();
            var clone = angular.copy(naIdentity.currentUser);
            angular.extend(clone, newUserData);
            clone.$update().then(function () {
                naIdentity.currentUser = clone;
                dfd.resolve();
            }, function (response) {
                dfd.reject(response.data.reason);
            });
            return dfd.promise;
        }
    }
});