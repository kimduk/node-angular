/**
 * Created by kimduk on 28.05.14.
 */

angular.module('app').factory('naIdentity', function ($window, naUser) {
    var currentUser;
    if(!!$window.bootstrappedUserObject) {
        currentUser = new naUser();
        angular.extend(currentUser, $window.bootstrappedUserObject);
    }
    return {
        currentUser: currentUser,
        isAuthenticated: function () {
            console.log(this.currentUser);
            return !!this.currentUser;
        },
        isAuthorized: function (role) {
            return !!this.currentUser && this.currentUser.roles.indexOf(role) > -1;
        }
    }
});