/**
 * Created by kimduk on 03.06.14.
 */

angular.module('app').controller('naSignupCtrl', function ($scope, naAuth, $location, naNotifier) {

    $scope.signup = function () {
        var newUserData = {
            username: $scope.email,
            password: $scope.password,
            firstName: $scope.fname,
            lastName: $scope.lname
        };

        naAuth.createUser(newUserData).then(function () {
            naNotifier.notify('User account create');
            $location.path('/');
        }, function(reason) {
            naNotifier.error(reason);
        });
    }
});