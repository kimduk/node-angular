/**
 * Created by kimduk on 04.06.14.
 */

angular.module('app').controller('naProfileCtrl', function ($scope, naAuth, naIdentity, naNotifier) {
    $scope.email = naIdentity.currentUser.username;
    $scope.fname = naIdentity.currentUser.firstName;
    $scope.lname = naIdentity.currentUser.lastName;

    $scope.update = function () {
        var newUserData = {
            username: $scope.email,
            firstName: $scope.fname,
            lastName: $scope.lname
        }


        if ($scope.password && $scope.password.length > 0) {
            newUserData.password = $scope.password;
        }

        naAuth.updateCurrentUser(newUserData).then(function() {
            naNotifier.notify('Your profile information has been update');
        }, function (reason) {
            naNotifier.error(reason);
        })
    }
})