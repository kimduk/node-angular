/**
 * Created by kimduk on 27.05.14.
 */
angular.module('app').controller('naNavBarLoginCtrl', function($scope, $http, naIdentity, naNotifier, naAuth, $location) {
    $scope.identity = naIdentity;
    $scope.signIn = function (username, password) {
        naAuth.authenticateUser(username, password).then(function(success) {
            if (success) {
                naNotifier.notify('You have Login!');
            } else {
                naNotifier.notify('Incorrect Login!');
            }
        });
    }

    $scope.signOut = function () {
        naAuth.logoutUser().then(function () {
            $scope.username = "";
            $scope.password = "";
            naNotifier.notify('You are successfully sighed out!');
            $location.path('/');
        });
    }
});