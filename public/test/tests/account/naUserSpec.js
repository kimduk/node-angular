/**
 * Created by kimduk on 30.05.14.
 */

describe('naUser', function() {
    beforeEach(module('app'));

    describe('isAdmin', function () {
        it('should return FALSE if the roles array does not have an admin entry', inject(function (naUser) {
            var user = new naUser();
            user.roles = ['not admin'];
            expect(user.isAdmin()).to.be.false;
        }));

        it('should return TRUE if the roles array have an admin entry', inject(function (naUser) {
            var user = new naUser();
            user.roles = ['admin'];
            expect(user.isAdmin()).to.be.true;
        }));
    });
});